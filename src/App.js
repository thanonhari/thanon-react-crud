import './App.css';
import React, { Component } from 'react';

import Navbar from './Navbar';
import Users from './Users';

function App() {
  return (
    <div>
      <Navbar />
      <Users />
    </div>
  );
}

export default App;